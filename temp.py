import requests
import json
response = requests.get("https://api.bitbucket.org/2.0/repositories/vargasmanuel/practicagit/commits")

i = 0
Johan = 0
Manuel = 0
info = json.loads(response.content)

for values in info['values']:
    if(values['type'] == "commit"):
        i = i + 1
    author = values['author']
    raw = author['raw']
    if(author['raw'] == "johanvalle12 <johan.valle@uabc.edu.mx>"):
        Johan = Johan + 1
    if(author['raw'] == "Manuel Vargas <vargas.manuel@uabc.edu.mx>"):
        Manuel = Manuel + 1
        
print("Cantidad de commits: " + str(i))
print("Cantidad de commits de Johan: " + str(Johan))
print("Cantidad de commits de Manuel: " + str(Manuel))